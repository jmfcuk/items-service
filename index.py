# from random import randint
from flask import Flask, request, jsonify, abort, make_response, url_for
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

items = []

def reset_items():

    its = []

    cat = 1
    for i in range(1, 9):
        its.append(
            {
                'id': i,
                'category': 'cat{}'.format(cat),
                'name': 'Item {}'.format(i),
                'description': 'This is a description of Item {}'.format(i), 
                'data': {'data1': 'data 1', 'data2': 'data 2', 'data3': 'data 3'},
                'imageUri': 'http://localhost:3000/img/cards/{}.jpg'.format(i),
                'itemUri': 'item-uri'
            }
        )

        cat += 1
        
        if cat > 3:
            cat = 1
    
    return its

def get_items_headers():
    headers = []

    for item in items:
        new_item = {}
        for field in item:
            if field != 'data' and field != 'itemUri':
                new_item[field] = item[field]
        headers.append(set_item_uri(new_item))

    return headers

def get_item_headers(item_id):
    item = find_item(item_id)
    new_item = {}
    for field in item:
        if field != 'data' and field != 'itemUri':
            new_item[field] = item[field]

    return set_item_uri(new_item)

def find_item(item_id):
    item = next((i for i in items if i['id'] == item_id), None)
    if not item:
        abort(404)
    return item

def set_item_uri(item):
    new_item = {}
    new_item['itemUri'] = '{}{}/{}'.format(url_for('index', _external=True), 'items', str(item['id']))
    for field in item:
        if field != 'itemUri':
            new_item[field] = item[field]
    return new_item

@app.route('/', methods=['GET'])
def index():
    return 200

@app.route('/reset')
def reset():
    global items
    items = reset_items()
    return jsonify({'items': get_items_headers()})

@app.route('/items/headers', methods=['GET'])
def get_all_headers():
    return jsonify({'items': get_items_headers()})

@app.route('/items/<int:item_id>/headers', methods=['GET'])
def get_headers(item_id):
    return jsonify({'items': get_item_headers(item_id)})

@app.route('/items', methods=['GET'])
def get_items():
    return jsonify({'items': [set_item_uri(item) for item in items]})
    
@app.route('/items/<int:item_id>', methods=['GET'])
def get_item(item_id):
    item = find_item(item_id)
    return jsonify({'item': set_item_uri(item)})

@app.route('/items', methods=['POST'])
def create_item():
    new_id = 1
    if items:
        new_id = items[-1]['id'] + 1
    item = {
        'id': new_id,
        'name': '{} {}'.format(request.json['name'], new_id),
        'description': request.json.get('description', ""),
        'data': request.json.get('data')
    }
    items.append(item)
    return jsonify({'item': set_item_uri(item)}), 201

@app.route('/items/<int:item_id>', methods=['PUT'])
def update_item(item_id):
    item = find_item(item_id)
    item['name'] = request.json.get('name', item['name'])
    item['description'] = request.json.get('description', item['description'])
    item['data'] = request.json.get('data', item['data'])
    return jsonify({'item': set_item_uri(item)})

@app.route('/items/<int:item_id>', methods=['DELETE'])
def delete_item(item_id):
    global items
    item = find_item(item_id)
    items.remove(item)
    return jsonify({'success': True})

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'404': 'Not found'}), 404)

@app.errorhandler(500)
def server_error(error):
    return make_response(jsonify({'500': 'Internal Server Error'}), 500)

if __name__ == '__main__':
    items = reset_items()
    app.run(debug=True)
